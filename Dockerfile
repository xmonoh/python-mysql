FROM python:3.6.5-alpine3.7
MAINTAINER Philipp Gadow <Paul.Philipp.Gadow@cern.ch>

COPY requirements.txt /requirements.txt

RUN apk add --update --no-cache mariadb-client-libs \
    && apk add --no-cache --virtual .build-deps \
        mariadb-dev \
        gcc \
        musl-dev \
    && pip install -r /requirements.txt \
    && apk del .build-deps

COPY scripts/ /app
WORKDIR /app

CMD ["/bin/sh"]