import re
import json
from argparse import ArgumentParser


def getArgs():
    """ Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument('inputFile', help='Text file containing cutflow generated with the XAMPP framework')
    parser.add_argument('-o', '--outputFile', default=None, help='JSON file containing cutflow extracted from input file')
    return parser.parse_args()


def process_line(line):
    """Process line from cutflow text file to extract cut name, event yield after cut and uncertainty on yield."""
    # first try XAMPPplotting format
    match = re.search(r'([0-9\.]+\s+\+-\s+[0-9\.]+)', line)
    if match:
        result = match.group(1)
        cut = line.replace(result, '').strip()
        result = result.split('+-')
        yld = float(result[0])
        err = float(result[1])
        return cut, yld, err
    else:
        # then try XAMPPbase format
        match = re.search(r'([0-9\.]+\s+±\s+[0-9\.]+)', line)
        if match:
            result = match.group(1)
            cut = line.replace(result, '').strip()
            result = result.split('±')
            yld = float(result[0])
            err = float(result[1])
            return cut, yld, err
        else: 
            return None

def extract_cutflow(pathToFile):
    """Extract cutflow objects from formatted text file and return as json object."""
    cutflow = {}
    with open(pathToFile, 'r') as inputFile:
        for row in inputFile:
            # clean input
            row = row.strip()
            # extract cut name, yield, and uncertainty and store in dictionary
            result = process_line(row)
            if result:
                cut, yld, err = result
                cutflow[cut] = [yld, err]
    return json.dumps(cutflow)


if __name__ == '__main__':
    # get arguments from command line
    args = getArgs()

    # create cutflow
    cutflow = extract_cutflow(args.inputFile)
    print(cutflow)

    # write cutflow to file (if specified)
    if args.outputFile:
        with open(args.outputFile, 'w') as outputFile:
            outputFile.write(cutflow)

