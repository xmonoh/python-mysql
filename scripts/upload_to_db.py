#!/usr/bin/python
import os
import MySQLdb
from argparse import ArgumentParser


def getArguments():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument('--commit', required=True, help='commit hash for referencing cutflows')
    parser.add_argument('--message', default='', help='commit message for referencing cutflows')
    parser.add_argument('--time', default='', help='commit time for referencing cutflows')

    parser.add_argument('--base_dt0lm', help='path to JSON file with XAMPPbase cutflow for data 0 lepton merged')
    parser.add_argument('--base_dt0lr', help='path to JSON file with XAMPPbase cutflow for data 0 lepton resolved')
    parser.add_argument('--base_dt1lm', help='path to JSON file with XAMPPbase cutflow for data 1 lepton merged')
    parser.add_argument('--base_dt1lr', help='path to JSON file with XAMPPbase cutflow for data 1 lepton resolved')
    parser.add_argument('--base_dt2lm', help='path to JSON file with XAMPPbase cutflow for data 2 lepton merged')
    parser.add_argument('--base_dt2lr', help='path to JSON file with XAMPPbase cutflow for data 2 lepton resolved')
    parser.add_argument('--base_dt2lelm', help='path to JSON file with XAMPPbase cutflow for data 2 lepton electron channel merged')
    parser.add_argument('--base_dt2lelr', help='path to JSON file with XAMPPbase cutflow for data 2 lepton electron channel resolved')
    parser.add_argument('--base_dt2lmum', help='path to JSON file with XAMPPbase cutflow for data 2 lepton muon channel merged')
    parser.add_argument('--base_dt2lmur', help='path to JSON file with XAMPPbase cutflow for data 2 lepton muon channel resolved')

    parser.add_argument('--plot_dt0lm', help='path to JSON file with XAMPPplotting cutflow for data 0 lepton merged')
    parser.add_argument('--plot_dt0lr', help='path to JSON file with XAMPPplotting cutflow for data 0 lepton resolved')
    parser.add_argument('--plot_dt1lm', help='path to JSON file with XAMPPplotting cutflow for data 1 lepton merged')
    parser.add_argument('--plot_dt1lr', help='path to JSON file with XAMPPplotting cutflow for data 1 lepton resolved')
    parser.add_argument('--plot_dt2lm', help='path to JSON file with XAMPPplotting cutflow for data 2 lepton merged')
    parser.add_argument('--plot_dt2lr', help='path to JSON file with XAMPPplotting cutflow for data 2 lepton resolved')
    parser.add_argument('--plot_dt2lelm', help='path to JSON file with XAMPPplotting cutflow for data 2 lepton electron channel merged')
    parser.add_argument('--plot_dt2lelr', help='path to JSON file with XAMPPplotting cutflow for data 2 lepton electron channel resolved')
    parser.add_argument('--plot_dt2lmum', help='path to JSON file with XAMPPplotting cutflow for data 2 lepton muon channel merged')
    parser.add_argument('--plot_dt2lmur', help='path to JSON file with XAMPPplotting cutflow for data 2 lepton muon channel resolved')

    return parser.parse_args()

def setupDB():
    """Connect to database, parameters specified by environment variables, need to be defined
       e.g. by export DB_HOST=localhost before calling this function."""
    db=MySQLdb.connect(
        host=os.environ['DB_HOST'],
        user=os.environ['DB_USER'],
        passwd=os.environ['DB_PWD'],
        db=os.environ['DB_NAME'])
    return db


def getFileContent(filePath):
    """Get content of file as string."""
    try:
        with open(filePath, 'r') as fileContent:
            return fileContent.read()
    except Exception:
        print('getFileContent: ERROR while trying to open {filepath}'.format(filepath=filePath))
        return "{}"


def uploadCutflows(db, metadata, cutflows):
    """Upload cutflows stored in dictionary cutflows to database db."""
    c=db.cursor()
    command = """INSERT INTO cutflows (commit, message, time, base_dt0lm, base_dt0lr, base_dt1lm, base_dt1lr, base_dt2lm, base_dt2lr, base_dt2lelm, base_dt2lelr, base_dt2lmum, base_dt2lmur, plot_dt0lm, plot_dt0lr, plot_dt1lm, plot_dt1lr, plot_dt2lm, plot_dt2lr, plot_dt2lelm, plot_dt2lelr, plot_dt2lmum, plot_dt2lmur)
    VALUES ('{commit}', '{message}', '{time}', '{base_dt0lm}', '{base_dt0lr}', '{base_dt1lm}', '{base_dt1lr}', '{base_dt2lm}', '{base_dt2lr}', '{base_dt2lelm}', '{base_dt2lelr}', '{base_dt2lmum}', '{base_dt2lmur}', '{plot_dt0lm}', '{plot_dt0lr}', '{plot_dt1lm}', '{plot_dt1lr}', '{plot_dt2lm}', '{plot_dt2lr}', '{plot_dt2lelm}', '{plot_dt2lelr}', '{plot_dt2lmum}', '{plot_dt2lmur}');""".format(
        commit=metadata['commit'],
        message=metadata['message'].replace('\'', ''),
        time=metadata['time'],
        base_dt0lm=cutflows['base_dt0lm'],
        base_dt0lr=cutflows['base_dt0lr'],
        base_dt1lm=cutflows['base_dt1lm'],
        base_dt1lr=cutflows['base_dt1lr'],
        base_dt2lm=cutflows['base_dt2lm'],
        base_dt2lr=cutflows['base_dt2lr'],
        base_dt2lelm=cutflows['base_dt2lelm'],
        base_dt2lelr=cutflows['base_dt2lelr'],
        base_dt2lmum=cutflows['base_dt2lmum'],
        base_dt2lmur=cutflows['base_dt2lmur'],
        plot_dt0lm=cutflows['plot_dt0lm'],
        plot_dt0lr=cutflows['plot_dt0lr'],
        plot_dt1lm=cutflows['plot_dt1lm'],
        plot_dt1lr=cutflows['plot_dt1lr'],
        plot_dt2lm=cutflows['plot_dt2lm'],
        plot_dt2lr=cutflows['plot_dt2lr'],
        plot_dt2lelm=cutflows['plot_dt2lelm'],
        plot_dt2lelr=cutflows['plot_dt2lelr'],
        plot_dt2lmum=cutflows['plot_dt2lmum'],
        plot_dt2lmur=cutflows['plot_dt2lmur']
        )
    print(command)
    c.execute(command)
    db.commit()


def main():
    """Connect to mySQL database and upload cutflows as JSON objects."""
    args = getArguments()

    metadata = {
        "commit": args.commit,
        "message": args.message,
        "time": args.time
    }

    cutflows = {
        "base_dt0lm": getFileContent(args.base_dt0lm),
        "base_dt0lr": getFileContent(args.base_dt0lr),
        "base_dt1lm": getFileContent(args.base_dt1lm),
        "base_dt1lr": getFileContent(args.base_dt1lr),
        "base_dt2lm": getFileContent(args.base_dt2lm),
        "base_dt2lr": getFileContent(args.base_dt2lr),
        "base_dt2lelm": getFileContent(args.base_dt2lelm),
        "base_dt2lelr": getFileContent(args.base_dt2lelr),
        "base_dt2lmum": getFileContent(args.base_dt2lmum),
        "base_dt2lmur": getFileContent(args.base_dt2lmur),
        "plot_dt0lm": getFileContent(args.plot_dt0lm),
        "plot_dt0lr": getFileContent(args.plot_dt0lr),
        "plot_dt1lm": getFileContent(args.plot_dt1lm),
        "plot_dt1lr": getFileContent(args.plot_dt1lr),
        "plot_dt2lm": getFileContent(args.plot_dt2lm),
        "plot_dt2lr": getFileContent(args.plot_dt2lr),
        "plot_dt2lelm": getFileContent(args.plot_dt2lelm),
        "plot_dt2lelr": getFileContent(args.plot_dt2lelr),
        "plot_dt2lmum": getFileContent(args.plot_dt2lmum),
        "plot_dt2lmur": getFileContent(args.plot_dt2lmur)
    }
    db = setupDB()
    uploadCutflows(db, metadata, cutflows)


if __name__ == '__main__':
    main()
